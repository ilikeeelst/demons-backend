package demons

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class DemonsConfig {
    @Value("\${test.prop}")
    val testProperty: String? = ""
}