package demons.controllers


import demons.models.entities.DemonsUser
import demons.models.LoginResponse
import demons.models.PostUserResponse
import demons.services.AuthenticationService
import demons.services.DemonsUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/auth", produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
class AuthController {
    @Autowired
    lateinit var authenticationService: AuthenticationService

    @Autowired
    lateinit var demonsUserService: DemonsUserService

    @Value("\${jwt.headerstring}")
    val headerString: String = ""

    @Value("\${jwt.tokenprefix}")
    val tokenPrefix: String = ""

    @PostMapping("/login")
    fun authenticateUser(@Valid @RequestBody loginRequest: DemonsUser) : ResponseEntity<LoginResponse> {
        val token = authenticationService.getToken(
                loginRequest.email,
                loginRequest.password
        )
        return ResponseEntity
                .ok()
                .header(headerString, "$tokenPrefix $token")
                .body(LoginResponse(
                email = loginRequest.email,
                accessToken = token,
                tokenPrefix = "Bearer",
                expirationTime = 100000000,
                authenticated = true
        ))
    }

    @PostMapping("/signup")
    fun registerUser(@Valid @RequestBody signupRequest: DemonsUser)
            : ResponseEntity<PostUserResponse> {
        val addedUser = demonsUserService.addUser(DemonsUser(signupRequest.email, signupRequest.password))
        return ResponseEntity.status(HttpStatus.CREATED).body(PostUserResponse(addedUser.email))
    }
}