package demons.controllers

import demons.models.ErrorReponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.servlet.http.HttpServletRequest

@ControllerAdvice
class ExceptionsController : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = arrayOf(BadCredentialsException::class, UsernameNotFoundException::class))
    fun authenticationWithBadCredentials(request: HttpServletRequest, ex: BadCredentialsException)
            : ResponseEntity<ErrorReponse> {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                ErrorReponse(
                        ex.message!!
                )
        )
    }

    @ExceptionHandler(value = arrayOf(RuntimeException::class))
    fun serverRuntimeError(request: HttpServletRequest, ex: RuntimeException)
            : ResponseEntity<ErrorReponse> {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorReponse(
                        ex.message!!
                )
        )
    }
}