package demons.controllers

import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import demons.services.ImageService
import demons.imagemanager.exceptions.ImageUploadException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import javax.validation.Valid
import demons.imagemanager.exceptions.ImageValidationException
import demons.models.*
import demons.models.entities.Image
import org.springframework.dao.DataIntegrityViolationException
import javax.validation.ConstraintViolationException

@RestController
@RequestMapping("/images", produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
class ImageCRUDController {

    @Autowired lateinit var imageService: ImageService

    @GetMapping()
    fun getImagesList() : ResponseEntity<GetImagesResponse> {
        return ResponseEntity.ok()
                .body(GetImagesResponse(imageService.getImagesList()))
    }

    @PostMapping()
    fun addNewImage(@Valid @RequestBody request: PostImageRequest) :
            ResponseEntity<PostImageResponse> {
        val result = imageService
                .addNewImage(request.base64EncodedImage, request.name)
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(PostImageResponse(id = result.id, url = result.url))
    }

    @GetMapping("/image/{id}")
    fun getImage(@PathVariable id: String): ResponseEntity<Image> {
        val image = imageService.getImageById(id)
        return ResponseEntity.status(HttpStatus.OK).body(image)
    }

    @ExceptionHandler(MissingKotlinParameterException::class)
    fun missingParameter (exception: MissingKotlinParameterException)
            :ResponseEntity<PostImageErrorResponse> {
        return (ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(PostImageErrorResponse(exception.msg)))
    }

    @ExceptionHandler(ConstraintViolationException::class, DataIntegrityViolationException::class)
    fun constraintViolation (exception: Exception)
            : ResponseEntity<PostImageErrorResponse> {
        return (ResponseEntity.status(HttpStatus.CONFLICT)
                .body(PostImageErrorResponse (exception.message!!)))
    }

    @ExceptionHandler(NoSuchElementException::class)
    fun failedToRetrieveById(exception: NoSuchElementException): ResponseEntity<ImageNotFoundResponse> {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ImageNotFoundResponse())
}
    @ExceptionHandler(ImageUploadException::class)
    fun uploadImageError (exception: ImageUploadException)
            : ResponseEntity<PostImageErrorResponse> {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(PostImageErrorResponse(exception.message))
    }

    @ExceptionHandler(ImageValidationException::class)
    fun imageValidationError (exception: ImageValidationException)
            : ResponseEntity<PostImageErrorResponse> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(PostImageErrorResponse(exception.message))
    }
}
