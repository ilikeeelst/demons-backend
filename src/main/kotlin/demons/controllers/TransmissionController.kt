package demons.controllers

import demons.models.LaunchResponse
import demons.models.SimulationPostTransmissionRequest
import demons.models.TestBedPostTransmissionRequest
import demons.services.TransmissionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/transmission", produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
class TransmissionController {
    @Autowired
    private lateinit var transmissionService: TransmissionService

    @PostMapping("/simulation")
    fun launchSimulationExecution(@Valid @RequestBody request: SimulationPostTransmissionRequest) : ResponseEntity<LaunchResponse> {
        val data = transmissionService.launchTransmission(request)
        return ResponseEntity.ok().body(LaunchResponse(data))
    }

    @PostMapping("/testbed")
    fun launchTestbedExecution(@Valid @RequestBody request: TestBedPostTransmissionRequest) : ResponseEntity<LaunchResponse> {
        val data = transmissionService.launchTransmission(request)
        return ResponseEntity.ok().body(LaunchResponse(data))
    }
}