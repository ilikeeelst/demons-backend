package demons.imagemanager

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import java.io.File
import java.io.IOException
import java.time.Instant
import java.time.format.DateTimeFormatter

@Configuration
@Component
class ImageKeeper {
    @Value("\${images.folder.location}")
    val imagesLocationFolder: String = "/"

    fun save(base64EncodedImage: String, name: String) :ImageSaveToDiskResult{
        val bytes = getBytes(base64EncodedImage)
        val path = imagesLocationFolder +
                DateTimeFormatter.ISO_INSTANT.format(Instant.now()) +
                name
        try {
            File(path).writeBytes(bytes)
        }
        catch (e:IOException) {
            return ImageSaveToDiskResult(success = false)
        }
        return ImageSaveToDiskResult(success = true, path = path)

    }
}
