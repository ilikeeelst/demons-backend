package demons.imagemanager

data class ImageUploadResult(val success: Boolean, val url: String = "", val id: String="")

data class ImageSaveToDiskResult(val success: Boolean, val path: String="")

data class AddImageResult(val id: String, val url: String)
