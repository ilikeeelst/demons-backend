package demons.imagemanager

import org.json.JSONObject
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class ImageUploader {
    @Value("\${imgur.endpoint}")
    val url: String = ""

    @Value("\${imgur.clientid}")
    val clientID: String = ""

    @Value("\${imgur.accesstoken}")
    val secret: String = ""

    @Value("\${imgur.demons.album}")
    val album: String = ""

    fun loadedSuccessfully(responseBody: JSONObject): Boolean {
        return responseBody["success"] == true
    }

    fun tokenExpired(responseBody: JSONObject): Boolean {
        return false //TODO what if token expires
    }

    fun getLink(responseBody: JSONObject): String {
        val link = responseBody.getJSONObject("data").get("link")
        return link.toString()
    }

    fun getId(responseBody: JSONObject): String {
        val id = responseBody.getJSONObject("data").get("id")
        return id.toString()
    }

    fun upload(base64EncodedImage: String, filename: String): ImageUploadResult {
        val payload = mapOf<String, Any>(
                "image" to base64EncodedImage,
                "album" to album,
                "name" to filename
        )
        val headers = mapOf<String, String>(
                "Authorization" to "Bearer " + secret
        )
        val response = khttp.post(
                url = url,
                data = payload,
                headers = headers
        )

        val responseBody = response.jsonObject

        if (response.statusCode == 200 && loadedSuccessfully(responseBody)) {
            return ImageUploadResult(true, getLink(responseBody), getId(responseBody))
        } else {
            return ImageUploadResult(false)
        }
    }

}