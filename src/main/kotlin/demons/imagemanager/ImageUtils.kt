package demons.imagemanager

import java.io.File
import java.util.*

fun getBase64Image(imageFile: File): String {
    val bytes = imageFile.readBytes()
    return Base64.getEncoder().encodeToString(bytes)
}

fun getBytes(base64EncodedImage: String): ByteArray {
    val bytes = Base64.getDecoder().decode(base64EncodedImage)
    return bytes
}

private fun isBase64Encoded(string: String) : Boolean {
    try {
        val bytes = Base64.getDecoder().decode(string)
        return true
    }
    catch (ex: IllegalArgumentException) {
        return false
    }
}

fun imageIsValid(encodedImage: String): Boolean {
    return isBase64Encoded(encodedImage) //TODO verify (limit) size
}