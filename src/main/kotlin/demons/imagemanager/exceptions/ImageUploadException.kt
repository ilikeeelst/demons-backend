package demons.imagemanager.exceptions

class ImageUploadException(override var message: String): Exception(message)