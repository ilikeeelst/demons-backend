package demons.imagemanager.exceptions

class ImageValidationException(override var message: String) : Exception(message)