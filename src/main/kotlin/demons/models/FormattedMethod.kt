package demons.models

import matlabdriver.models.Snr


data class FormattedChannel (
        val timeValues: Array<Double>,
        val frequencyValues: Array<Double>,
        val amplitudes: Array<Array<Double>>
)

data class FormattedSnr (
        val calculatedSNR: Double,
        val ssim: Double,
        val sdr: Double,
        val outputImage: String,
        val channel: FormattedChannel

)

data class FormattedMethod (
        val strategy: List<String>,
        val SNRs: List<FormattedSnr>
)
