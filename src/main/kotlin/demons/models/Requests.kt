package demons.models



data class PostImageRequest (val name: String, val base64EncodedImage: String)


abstract class PostTransmissionRequest(
        val environment: String,
        val imageId:  String,
        val compareWithDigital:  Boolean
)

class SimulationPostTransmissionRequest(
        environment: String,
        imageId: String,
        compareWithDigital: Boolean,
        val snr: Double,
        val channelType: String,
        val transmissionType: String
) : PostTransmissionRequest(environment, imageId, compareWithDigital)

class TestBedPostTransmissionRequest(
        environment: String,
        imageId: String,
        compareWithDigital: Boolean,
        val txGain: Double,
        val rxGain: Double,
        val carrierFrequency: Int
) : PostTransmissionRequest(environment, imageId, compareWithDigital)

