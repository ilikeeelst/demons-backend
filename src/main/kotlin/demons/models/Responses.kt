package demons.models

import demons.models.entities.Image


data class GetImagesResponse(val images: List<Image>
)

data class PostImageResponse(val id: String,
                             val url: String,
                             val created: Boolean = true)

data class PostImageErrorResponse(val message: String, val created: Boolean = false)

data class ImageNotFoundResponse(val found: Boolean = false)

data class PostUserResponse(val email: String, val created: Boolean = true)


data class LoginResponse(val email: String,
                         val accessToken: String,
                         val tokenPrefix: String,
                         val expirationTime: Long = 0,
                         val message: String = "Authentication successful",
                         val authenticated: Boolean= true)

data class ErrorReponse(
        val message: String)

data class LaunchResponse(
        val data: List<FormattedMethod>,
        val success: Boolean = true
)
