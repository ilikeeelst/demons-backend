package demons.models.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigInteger
import javax.validation.constraints.Email


data class DemonsUser(
        @Indexed(unique = true) @Email val email : String,
        val password : String) {
    @Id lateinit var id: String
}

