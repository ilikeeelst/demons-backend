package demons.models.entities

import org.springframework.data.annotation.Id

data class Image(
                 val name: String,
                 val path: String,
                 val url: String) {
    @Id lateinit var id: String
}
