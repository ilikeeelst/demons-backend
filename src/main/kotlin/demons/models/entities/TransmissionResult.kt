package demons.models.entities

import demons.models.FormattedMethod
import org.springframework.data.mongodb.core.mapping.Document

data class TransmissionResult(
        val username: String,
        val methods: List<FormattedMethod>,
        val environment: String,
        val imageId: String,
        val compareWithDigital: Boolean
)
