package demons.repositories

import demons.models.entities.DemonsUser
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.math.BigInteger
import java.util.*

@Repository
interface DemonsUserRepository : CrudRepository <DemonsUser, String> {
    fun findByEmail (email : String) : Optional<DemonsUser>
}
