package demons.repositories

import demons.models.entities.TransmissionResult
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository


@Repository
interface TrasmissionResultRepository : CrudRepository<TransmissionResult, Long> {
    fun findByUsername(username: String)
}
