package demons.security

import demons.services.DemonsUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JwtAuthenticationFilter : OncePerRequestFilter() {
    @Autowired
    lateinit var demonsUserDetailsService: DemonsUserDetailsService

    @Autowired
    lateinit var jwtManager: JwtManager

    @Value("\${jwt.tokenprefix}")
    val tokenPrefix: String = "Bearer"

    @Value("\${jwt.headerstring}")
    val headerString: String = "Authorization"

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val token = getTokenFromRequest(request)
        if (StringUtils.hasText(token) && jwtManager.isValid(token)) {
            val username = jwtManager.getSubjectfromJWT(token)
            val userDetails = demonsUserDetailsService.loadUserByUsername(username)
            val authenticationToken = UsernamePasswordAuthenticationToken(
                    userDetails,
                    null,
                    userDetails.authorities
            )
            authenticationToken.details= WebAuthenticationDetailsSource().buildDetails(request)
            SecurityContextHolder.getContext().authentication = authenticationToken
        }

        filterChain.doFilter(request, response)
    }

    fun getTokenFromRequest(request: HttpServletRequest) : String {
        val authorizationHeaderValue = request.getHeader(headerString)
        if (authorizationHeaderValue != null && authorizationHeaderValue.startsWith(tokenPrefix)) {
            val token = authorizationHeaderValue
                    .replace(tokenPrefix, "")
                    .replace(" ", "")
            return token
        }
        return ""
    }
}
