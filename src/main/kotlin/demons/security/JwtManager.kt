package demons.security

import io.jsonwebtoken.*
import io.jsonwebtoken.impl.crypto.MacProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.logging.Logger
import javax.crypto.SecretKey
import javax.servlet.http.HttpServletRequest

@Configuration
@Component
class JwtManager {
    val logger = Logger.getLogger(this.javaClass.name)

    val mainKey: SecretKey = MacProvider.generateKey(SignatureAlgorithm.HS512)

    fun generateToken(subject: String, validityTime: Long) : String {
        val expirationDate = ZonedDateTime.
                now(ZoneOffset.UTC)
                .plus(validityTime, ChronoUnit.MILLIS)
        val token = Jwts.builder()
                .setSubject(subject)
                .setExpiration(Date.from(expirationDate.toInstant()))
                .signWith(SignatureAlgorithm.HS512, mainKey)
                .compact()
        return token
    }

    fun isValid (token: String) : Boolean {
        try {
            Jwts.parser().setSigningKey(mainKey)
                    .parseClaimsJws(token)
            return true
        }catch (ex: SignatureException) {
            logger.warning("Invalid JWT signature")
        } catch (ex: MalformedJwtException) {
            logger.warning("Invalid JWT token")
        } catch (ex: ExpiredJwtException) {
            logger.warning("Expired JWT token")
        } catch (ex: UnsupportedJwtException) {
            logger.warning("Unsupported JWT token")
        } catch (ex: IllegalArgumentException) {
            logger.warning("JWT claims string is empty or invalid.")
        }
        return false
    }

    fun getSubjectfromJWT(token: String) : String{
        val claims = Jwts.parser()
                .setSigningKey(mainKey)
                .parseClaimsJws(token)
                .body
        return claims.subject
    }
}