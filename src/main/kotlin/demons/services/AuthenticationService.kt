package demons.services

import demons.security.JwtManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Service
import java.util.logging.Logger

@Service
class AuthenticationService {
    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var jwtManager: JwtManager

    @Value("\${jwt.validitytime}")
    val validityTime: Long = 0

    val logger = Logger.getLogger(this.javaClass.name)

    private fun getAuthentication(username: String, password: String): Authentication {
        try {
            val authentication = authenticationManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            username,
                            password
                    )
            )
            return authentication
        } catch (ex: Exception) {
            val message = "Could not authenticate user: $username\n" +
                    "It seems like the user trying to authenticate is not registered"
            logger.info(message)
            throw BadCredentialsException(message, ex)
        }
    }

    fun getToken(username: String, password: String): String {
        val authentication = getAuthentication(username, password)
        SecurityContextHolder.getContext().authentication = authentication
        val user = authentication.principal
        if (user is User) {
            val token = jwtManager.generateToken(user.username, validityTime)
            return token
        } else {
            val message = "The authentication principal has an unexpected format"
            logger.severe(message)
            throw RuntimeException(message)
        }
    }
}