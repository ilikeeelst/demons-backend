package demons.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class DemonsUserDetailsService : UserDetailsService {
    @Autowired
    lateinit var demonsUserService: DemonsUserService

    override fun loadUserByUsername(username: String?): UserDetails {
        if (username == null || username == "") {
            val message = "The given username has not been found in the database"
            throw UsernameNotFoundException(message)
        }
        else{
            val user = demonsUserService.getUserByEmail(username)
            return User(user.email, user.password, AuthorityUtils.createAuthorityList("USER"))
        }

    }
}