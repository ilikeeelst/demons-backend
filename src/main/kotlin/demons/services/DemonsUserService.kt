package demons.services

import demons.models.entities.DemonsUser
import demons.repositories.DemonsUserRepository
import demons.security.SecurityConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class DemonsUserService : DemonsUserServiceInterface {
    @Autowired
    lateinit var demonsUserRepository: DemonsUserRepository

    override fun getUserbyId(id: String): DemonsUser {
        val storedUser = demonsUserRepository.findById(id)
        return storedUser.get()
    }

    override fun getUserWithEncodedPassword(user: DemonsUser): DemonsUser {
        return DemonsUser(
                email = user.email,
                //password = user.password
                password = SecurityConfig.passwordEncoder().encode(user.password)
        )
    }

    override fun addUser(user: DemonsUser): DemonsUser {
        val storedUser = demonsUserRepository.save(getUserWithEncodedPassword(user))
        return storedUser
    }

    override fun getUserByEmail(email: String): DemonsUser {
        val user = demonsUserRepository.findByEmail(email)
        return  user.get()
    }

}
