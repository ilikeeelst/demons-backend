package demons.services

import demons.models.entities.DemonsUser

interface DemonsUserServiceInterface {
    fun getUserbyId(id: String): DemonsUser

    fun getUserByEmail(email: String): DemonsUser

    fun addUser (user: DemonsUser) : DemonsUser

    fun getUserWithEncodedPassword(user: DemonsUser) : DemonsUser
}
