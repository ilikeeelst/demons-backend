package demons.services

import demons.imagemanager.AddImageResult
import demons.imagemanager.ImageKeeper
import demons.imagemanager.ImageUploader
import demons.imagemanager.exceptions.ImageValidationException
import demons.imagemanager.imageIsValid
import demons.models.entities.Image
import demons.repositories.ImageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ImageService {
    @Autowired
    lateinit var imageKeeper: ImageKeeper

    @Autowired
    lateinit var imageUploader: ImageUploader

    @Autowired
    lateinit var imageRepository: ImageRepository

    fun getImageById(id: String): Image {
        val image = imageRepository.findById(id)
        return image.get()
    }

    fun getImagesList(): List<Image> {
        val images = imageRepository.findAll()
        return images.toList()
    }

    fun addNewImage(base64EncodedImage: String, name: String) : AddImageResult {
        if (! imageIsValid(base64EncodedImage)) {
            //TODO
            throw ImageValidationException("Image received is not valid")
        }
        val saveResult = imageKeeper.save(base64EncodedImage, name)
        val uploadResult= imageUploader.upload(base64EncodedImage, name)
        val image = Image(name = name, path = saveResult.path, url = uploadResult.url)
        val savedImage = imageRepository.save(image)
        return AddImageResult(savedImage.id, uploadResult.url)
    }

}
