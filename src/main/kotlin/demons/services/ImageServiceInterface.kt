import demons.imagemanager.AddImageResult
import demons.models.entities.Image

interface ImageServiceInterface {
    fun getImageById(id: String): Image

    fun getImagesList(): List<Image>

    fun addNewImage(base64EncodedImage: String, name: String) : AddImageResult
}