package demons.services

import demons.getModule
import demons.imagemanager.getBase64Image
import demons.models.*
import demons.models.entities.TransmissionResult
import demons.repositories.TrasmissionResultRepository
import matlabdriver.managers.TransmissionManager
import matlabdriver.parameters.SimulationParameters
import matlabdriver.parameters.TestbedParameters
import matlabdriver.parameters.TransmissionParameters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Service

@Service
class TransmissionService {
    @Autowired
    private lateinit var imageService: ImageService

    @Autowired
    private lateinit var transmissionResultRepository: TrasmissionResultRepository

    private val trasmissionManager = TransmissionManager()

    fun launchTransmission (request: PostTransmissionRequest) : List<FormattedMethod> {
        val parameters = buildParameters(request
        )
        val tranmsissionResult = this.trasmissionManager.launchTransmission(parameters)
        val methods = arrayListOf<FormattedMethod>()
        for (method in tranmsissionResult.methods) {
            methods.add(FormattedMethod(
                    strategy = method.strategy,
                    SNRs = method.SNRs.map { FormattedSnr(
                            calculatedSNR = it.calculatedSNR,
                            ssim = it.ssim,
                            sdr = it.sdr,
                            channel = FormattedChannel(
                                    timeValues = it.channel.timeValues,
                                    frequencyValues = it.channel.frequencyValues,
                                    amplitudes = it.channel.values.map
                                    {row -> row.map { item -> getModule(item) }.toTypedArray()}.toTypedArray()
                            ),
                            outputImage = getBase64Image(it.outputImageFile)
                    )}
            ))
        }
        val username: String
        val principal = SecurityContextHolder.getContext().authentication.principal
        if (principal is User) {
            username = principal.username
        }
        else {
            throw AuthenticationServiceException(
                    "Bad credentials\n" +
                            principal.javaClass)
        }
        transmissionResultRepository.save(
                TransmissionResult(
                        username = username,
                        environment = request.environment,
                        imageId = request.imageId,
                        compareWithDigital = request.compareWithDigital,
                        methods = methods
                )
        )
        return methods
    }

    private fun buildParameters (request: PostTransmissionRequest): TransmissionParameters {
        val environment = request.environment
        val image = imageService.getImageById("")
        val compareWithDigital = request.compareWithDigital
        when (environment) {
            "simulation" -> {
                if (request is SimulationPostTransmissionRequest) {
                    return SimulationParameters(
                            imagePath = image.path,
                            compareWithDigital = compareWithDigital,
                            snr = request.snr,
                            channelType = request.channelType,
                            transmissionType = request.transmissionType
                    )
                }
                else {
                    throw RuntimeException()
                }
            }
            "testbed" -> {
                if (request is TestBedPostTransmissionRequest) {
                    return TestbedParameters(
                            imagePath= image.path,
                            compareWithDigital = compareWithDigital,
                            txGain = request.txGain,
                            rxGain = request.rxGain,
                            carrierFrequency = request.carrierFrequency
                    )
                }
                else {
                    throw RuntimeException()
                }

            }
            else -> throw NotImplementedError("environment $environment is not supported")
        }
    }
}
