package demons

import com.mathworks.matlab.types.Complex
import kotlin.math.sqrt

fun getModule(complex: Complex) : Double {
    return sqrt(complex.real*complex.real + complex.imag * complex.imag)
}