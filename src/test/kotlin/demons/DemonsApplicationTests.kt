package demons

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
class DemonsApplicationTests {

	@Autowired
	lateinit var config: DemonsConfig

	@Test
	fun contextLoads() {
	}

	@Test
	fun `configuration values can be accessed`() {
		assert(config.testProperty.equals("TEST"))
	}

}
