package demons.controllers

import demons.models.entities.DemonsUser
import demons.services.AuthenticationService
import demons.services.DemonsUserService
import org.json.JSONObject
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@ExtendWith(SpringExtension::class)
@WebMvcTest(controllers = [AuthController::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration
class AuthControllerTest {
    @MockBean
    private lateinit var userService : DemonsUserService

    @MockBean
    private lateinit var authenticationService: AuthenticationService

    @Autowired
    private lateinit var context: WebApplicationContext

    @Value("\${jwt.headerstring}")
    val headerString: String = ""

    @Value("\${jwt.tokenprefix}")
    val tokenPrefix: String = ""

    private lateinit var mockMvc: MockMvc

    val signupUrl = "/api/v1/auth/signup"
    val loginUrl = "/api/v1/auth/login"

    val email = "test@test.test"
    val badEmail = "bademail"
    val password = "password"
    val testUser = DemonsUser(email = email, password = password)
    val badEmailUser = DemonsUser(email = badEmail, password = password)
    val token = "atoken"

    @BeforeAll
    fun setUp (){
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                //.apply<DefaultMockMvcBuilder>(springSecurity())
                .build()
    }


    @Test
    fun `Sending POST to the signup endpoint with a valid json creates a new user`() {
        val payload = mapOf(
                "email" to email,
                "password" to password
        )
        Mockito.`when`(userService.addUser(testUser))
                .thenReturn(testUser)

        mockMvc.perform(MockMvcRequestBuilders.post(signupUrl)
                .with(csrf().asHeader())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JSONObject(payload).toString()))
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.created").value("true"))
                .andExpect(jsonPath("$.email").value(testUser.email))
    }

    @Test
    fun `sending POST with an existing user data gets an access token in the response`() {
        val payload = mapOf(
                "email" to email,
                "password" to password
        )

        Mockito.`when`(authenticationService.getToken(email, password))
                .thenReturn(token)
        mockMvc.perform(MockMvcRequestBuilders.post(loginUrl)
                .with(csrf().asHeader())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JSONObject(payload).toString()))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.header()
                        .string(
                                headerString,
                                tokenPrefix + " " + token
                        ))
                .andExpect(jsonPath("$.email").value(testUser.email))
                .andExpect(jsonPath("$.accessToken").value(token))
                .andExpect(jsonPath("$.authenticated").value(true))
                .andExpect(jsonPath("$.tokenPrefix").value(tokenPrefix))

    }
}