package demons.controllers

import demons.imagemanager.AddImageResult
import demons.models.entities.Image
import demons.services.ImageService
import org.json.JSONObject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import java.io.File
import java.util.*
import kotlin.NoSuchElementException
import org.mockito.Mockito.`when` as _when

@ExtendWith(SpringExtension::class)
@WebMvcTest(controllers = [ImageCRUDController::class])
class ImageCRUDControllerTest(@Autowired val mockMvc: MockMvc) {
    @MockBean
    private lateinit var imageService: ImageService

    val base64EncodedImage: String = Base64.getEncoder().encodeToString(File(
            "/home/tod/Pictures/demons/testimage.bmp"
    ).readBytes())
    val imageName = "testimage"
    val imageUrl = "http://test/image"
    val imagePath = "/image"

    @Test
    fun `The images endpoint replies to GET with a json containing a list of images`() {
        val image1 = Image(name = "image1", path = "/image1", url = imageUrl + 1)
        val image2 = Image(name = "image2", path = "/image2", url = imageUrl + 2)
        _when(imageService.getImagesList()).thenReturn(listOf(image1, image2))
        this.mockMvc.perform(get("/images").accept(MediaType.APPLICATION_JSON_UTF8)
                .with(user("user")))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.images").isArray)
                .andExpect(jsonPath("$.images[0].name").value("image1"))
                .andExpect(jsonPath("$.images[1].path").value("/image2"))
    }

    @Test
    fun `Sending POST to the images endpoint with a valid json creates a new image`() {
        val payload = mapOf<String, String>(
                "name" to imageName,
                "base64EncodedImage" to base64EncodedImage
        )
        _when(imageService.addNewImage(base64EncodedImage, imageName))
                .thenReturn(AddImageResult("", imageUrl))
        this.mockMvc.perform(post("/images")
                .with(csrf().asHeader())
                .with(user("user"))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JSONObject(payload).toString()))
                .andExpect(status().isCreated)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    }

    @Test
    fun `Sending POST to the images endpoint with a not-valid json returns a 400`() {
        this.mockMvc.perform(post("/images")
                .with(csrf().asHeader())
                .with(user("user"))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"names\": \"image\"}"))
                .andExpect(status().isBadRequest)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    }

    @Test
    fun `Trying to get an image by ID returns an image object correctly`() {
        val image = Image(name = imageName, url = imageUrl, path = imagePath)
        _when(imageService.getImageById(1.toString()))
                .thenReturn(image)
        this.mockMvc.perform(get("/images/image/1")
                .with(user("user")))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name"). value(image.name))

    }

    @Test
    fun `Trying to get a not existing image by ID returns 404`() {
        _when(imageService.getImageById(1.toString()))
                .thenThrow(NoSuchElementException())
        this.mockMvc.perform(get("/images/image/1")
                .with(user("user")))
                .andExpect(status().isNotFound)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    }

}