
import demons.controllers.TransmissionController
import demons.services.TransmissionService
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@ExtendWith(SpringExtension::class)
@WebMvcTest(controllers = [TransmissionController::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration
class TransmissionControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var context: WebApplicationContext

    @MockBean
    private lateinit var transmissionService: TransmissionService


    @BeforeAll
    fun setUp (){
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build()
    }


    @Test
    fun `Sending GET with valid data triggers an execution`() {

    }

    @Test
    fun `Sending GET with parameters out range receives a 400 as response`() {
    }
}
