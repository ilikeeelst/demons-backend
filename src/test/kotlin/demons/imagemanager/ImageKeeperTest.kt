package demons.imagemanager

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.io.File

@ExtendWith(SpringExtension::class)
@SpringBootTest
class ImageKeeperTest {
    @Autowired
    lateinit var imageKeeper: ImageKeeper

    @Test
    fun `Saves images to disk correctly`() {
        val imageFile = File("/home/tod/gtec/demoproject/demoajscc/analog_system/test_images/gray_scale/goldhill.bmp")
        val base64EncodedImage = getBase64Image(imageFile)
        val result = imageKeeper.save(base64EncodedImage, "goldhill.bmp")
        assert(result.success)
    }

}