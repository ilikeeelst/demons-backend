package demons.repositories

import demons.models.entities.DemonsUser
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DuplicateKeyException
import org.springframework.security.core.userdetails.User as UserDetails
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DemonsUserRepositoryTest {
    @Autowired
    private lateinit var demonsUserRepository : DemonsUserRepository

    private val password = "password"
    val user = DemonsUser("test@test.test", password)
    val user2 = DemonsUser("test2@test.test", password)

    @BeforeAll
    fun saveOneUser () {
        demonsUserRepository.save(user)
    }

    @Test
    fun `correctly stores a new user`() {
        val storedUser = demonsUserRepository.save(user2)
        assertThat(demonsUserRepository.findAll()).contains(storedUser)
    }

    @Test
    fun `correctly retrieves a user previously stored by its id`() {
        val storedUser = demonsUserRepository.save(user2)
        assertThat(demonsUserRepository.findById(storedUser.id)).hasValue(storedUser)
    }

    @Test
    fun `finds a user by its email` () {
        val storedUser = demonsUserRepository.findByEmail(user.email)
        assertThat(storedUser.get().email).isEqualTo(user.email)
    }

    @Test
    fun `refuses to store a user if has the same email of someone else throwing a MongoWriteException` () {
        val otherUser = DemonsUser(email = user.email, password = password)
        assertThrows<DuplicateKeyException> {demonsUserRepository.save(otherUser)}
    }

    @AfterAll
    fun cleanUp() {
        demonsUserRepository.deleteAll()
    }

}