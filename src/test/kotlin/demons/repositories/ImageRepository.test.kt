package demons.repositories

import demons.models.entities.Image
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.boot.test.context.SpringBootTest

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ImageRepositoryTest {
    @Autowired
    lateinit var imageRepository: ImageRepository

    private val image: Image

    init {
        image = Image("image.jpg", "/image.jpg", "http://test/image.jpg")
    }

    @Test
    fun `correctly stores a new image`() {
        imageRepository.save(image)
        assertThat(imageRepository.findAll()).contains(image)
    }

    @Test
    fun `finds a stored image by id` () {
        val storedImage = imageRepository.save(image)
        assertThat(imageRepository.findById(image.id)).hasValue(storedImage)
    }

    @AfterEach
    fun cleanUp() {
        imageRepository.deleteAll()
    }
}
