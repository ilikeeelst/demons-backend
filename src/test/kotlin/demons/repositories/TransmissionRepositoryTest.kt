package demons.repositories

import demons.models.entities.Image
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension


@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransmissionRepositoryTest {
    @Autowired
    lateinit var trasmissionResultRepository: TrasmissionResultRepository

    private val image: Image

    init {
        image = Image("image.jpg", "/image.jpg", "http://test/image.jpg")
    }


}


