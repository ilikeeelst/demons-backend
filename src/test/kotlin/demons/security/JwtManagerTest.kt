package demons.security

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JwtManagerTest {

    @Autowired
    lateinit var jwtManager: JwtManager

    @Value("\${jwt.validitytime}")
    val validityTime: Long = 0

    val testSubject = "subject"

    private lateinit var validToken: String
    private lateinit var expiredToken: String
    private lateinit var malformedtoken: String

    @BeforeAll
    fun createToken (){
        validToken = jwtManager.generateToken(testSubject, validityTime)
        expiredToken = jwtManager.generateToken(testSubject, 0)
        malformedtoken = "testbadtoken"
    }


    @Test
    fun `returns a jwt when asked`() {
    }

    @Test
    fun `correctly retrieves the subject from a token` () {
        val subject = jwtManager.getSubjectfromJWT(validToken)
        assertEquals(testSubject, subject)
    }

    @Test
    fun `says that a valid token is valid` () {
        val isValid = jwtManager.isValid(validToken)
        assertTrue(isValid)
    }

    @Test
    fun `says that an expired token is not valid` () {
        val isValid = jwtManager.isValid(expiredToken)
        assertFalse(isValid)
    }

    @Test
    fun `says that a malformed token with is not valid ` () {
        val isValid = jwtManager.isValid(malformedtoken)
        assertFalse(isValid)
    }
}