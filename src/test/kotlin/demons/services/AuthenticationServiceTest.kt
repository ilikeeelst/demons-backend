package demons.services

import demons.security.JwtManager
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuthenticationServiceTest {

    val username = "user"

    val password = "pass"


    @MockBean
    lateinit var authenticationManager: AuthenticationManager

    @MockBean
    lateinit var jwtManager: JwtManager

    @Autowired
    lateinit var authenticationService: AuthenticationService

    @Test
    fun `throws a BadCredentialsException if trying to get token with bad credentials`() {
        assertThrows<BadCredentialsException>{authenticationService.getToken(username, password)}
    }
}