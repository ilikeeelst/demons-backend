package demons.services

import demons.models.entities.DemonsUser
import demons.repositories.DemonsUserRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*
import org.mockito.Mockito.`when` as _when


@ExtendWith(SpringExtension::class)
@SpringBootTest
class DemonsUserServiceTest {

    private val password = "password"
    val user = DemonsUser("test@test.test", password)

    @MockBean
    lateinit var demonsUserRepository: DemonsUserRepository

    @Autowired
    lateinit var demonsUserService: DemonsUserService

    @Test
    fun `correctly finds a user by its email` () {
        _when(demonsUserRepository.findByEmail(user.email))
                .thenReturn(Optional.of(user))
        assertThat(demonsUserService.getUserByEmail(user.email)).isEqualTo(user)
    }

    @Test
    fun `correctly stores new users` () {
        _when(demonsUserRepository.save(any(DemonsUser::class.java)))
                .thenReturn(user)
        assertThat(demonsUserService.addUser(user)).isEqualTo(user)
    }

    @Test
    fun `the password is encoded before committing the new user` () {
        //TODO
        /*
        val storedUser = demonsUserService.addUser(user)
        assertThat(storedUser.password).isNotEqualTo(user.password)
        assertThat(storedUser.password).isEqualTo(SecurityConfig.passwordEncoder().encode(user.password))
        */
    }
}