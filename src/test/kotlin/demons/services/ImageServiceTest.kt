package demons.services

import demons.imagemanager.*
import demons.models.entities.Image
import demons.repositories.ImageRepository
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*
import org.mockito.Mockito.`when` as _when

@ExtendWith(SpringExtension::class)
@SpringBootTest
class ImageServiceTest {
    @Autowired
    lateinit var imageService: ImageService

    @MockBean
    lateinit var imageUploader: ImageUploader

    @MockBean
    lateinit var imageKeeper: ImageKeeper

    @MockBean
    lateinit var imageRepository: ImageRepository

    private val image = Mockito.mock(Image::class.java)
    private val image2 = Mockito.mock(Image::class.java)
    private val image3 = Image(
            "testimage",
            "/images/testimage",
            "http://testurl/testimage")
    val base64EncodedImage = "abc"
    private val imageId = "a"

    init {
        image3.id = imageId
    }

    @Test
    fun `correctly returns a stored image given a valid id`() {
        _when(image.id).thenReturn(imageId)
        _when(imageRepository.findById(image.id))
                .thenReturn(Optional.of(image))
        assert(imageService.getImageById(image.id).id.equals(image.id))
    }

    @Test
    fun `correctly returns the list of images in the database`() {
        _when(imageRepository.findAll())
                .thenReturn(listOf(image, image2))
        assert(imageService.getImagesList().containsAll(listOf(image, image2)))
    }

    @Test
    fun `correctly stores an image and returns its metadata` () {
        val keeperResult = ImageSaveToDiskResult(success = true, path = image3.path)
        _when(imageKeeper.save(
                base64EncodedImage = base64EncodedImage,
                name = image3.name)).thenReturn(keeperResult)

        _when(imageRepository.save(image3))
                .thenReturn(image3)
        _when(imageUploader.upload(base64EncodedImage, image3.name))
                .thenReturn(ImageUploadResult(
                        success = true,
                        url = image3.url,
                        id= image3.id))
        assert(imageService
                .addNewImage(base64EncodedImage, image3.name)
                .equals(AddImageResult(id= image3.id, url = image3.url)))
    }

    @AfterEach
    fun cleanUp ()  {
        imageRepository.deleteAll()
    }
}