package demons.services


import demons.models.entities.Image
import demons.models.SimulationPostTransmissionRequest
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransmissionServiceTest {

    @Autowired
    private lateinit var transmissionService: TransmissionService

    @MockBean
    private lateinit var imageService: ImageService


    val imagePath =  "/home/tod/Pictures/demons/testimage.bmp"
    private lateinit var request: SimulationPostTransmissionRequest

    val id = "a"


    @BeforeAll
    fun initData () {
        request = SimulationPostTransmissionRequest(
                environment = "simulation",
                imageId = id,
                compareWithDigital = false,
                snr = 10.0,
                channelType = "itur3GIAx",
                transmissionType = "pedestrian"
        )
    }



/*
    @Test
    fun `you can launch a new transmission providing valid parameters` () {
        Mockito.`when`(imageService.getImageById(id)).thenReturn(Image("testimage", imagePath, "", id))
        transmissionService.launchTransmission(request)
    }
    */


    @Test
    fun `the execution fails if you provide a not supported environment`() {
        Mockito.`when`(imageService.getImageById("")).thenReturn(Image("testimage", imagePath, ""))
        val request = SimulationPostTransmissionRequest(
                "s",
                id,
                false,
                10.0,
                "itur3GIAx",
                "static"
        )
        assertThrows<NotImplementedError> {transmissionService.launchTransmission(request)}
    }


    @Test
    fun `it throes a NotImplementedError if you provide a not supported channel type` () {
        Mockito.`when`(imageService.getImageById("")).thenReturn(Image("testimage", imagePath, ""))
        val request = SimulationPostTransmissionRequest(
                "simulation",
                id,
                false,
                10.0,
                "itur3GIA",
                "static"
        )
        assertThrows<NotImplementedError> {transmissionService.launchTransmission(request)}
    }

}
